import React from 'react';
import {BrowserRouter,Route} from 'react-router-dom';
import Navbar from "./components/Navbar"
import Home from "./components/home"
import Product from "./components/products"
import Cart from "./components/carts"
import ContactUs from "./components/contactUs"


function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <React.Fragment>
          <Navbar/>
          {/*Syntax:<Route path='<path specified>' component={component to be rendered if path changed}>*/}
          <Route exact path="/" component={Home}/>
          <Route exact path="/products" component={Product}/>
          <Route exact path="/carts" component={Cart}/>
          <Route exact path="/contactUs" component={ContactUs}/>
        </React.Fragment>
      </BrowserRouter>
    </div>
  )
}

export default App
