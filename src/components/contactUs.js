import React from "react"
import Form from "./Form"



const ContactUs = () => {
	return(
		<div className="container">
			<div className="row">
				<div className="col-12">
					<h1 className="text-center">
						Contact Us Page
					</h1>
				</div>
			</div>
			<hr/>	
			<div className="row">
				<div className="col-6">
					<Form/>
				</div>
				<div className="col-6">
						<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d15441.661680089886!2d121.0449656!3d14.632344500000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sph!4v1580373070332!5m2!1sen!2sph" width="600" height="450" frameborder="0" allowfullscreen=""></iframe>				  
				</div>
			</div>
		</div>
	)
}

export default ContactUs