import React from "react" 
import {Link,NavLink} from 'react-router-dom'

const Navbar = () => {
	let isLoggedIn = false;
	if (isLoggedIn) {
	return(
		<nav className="navbar navbar-expand-lg navbar-light bg-light">
			<div className="container">
				<a href="#" className="navbar-brand">Navbar</a>
				<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="content">
					<span className="navbar-toggler-icon"></span>
				</button>
				
				<div className="collapse navbar-collapse" id="content">
					<ul className="navbar-nav mr-auto">
						<li className="nav-item">
							<Link className="nav-link" to="/">Home</Link>
						</li>
						<li className="nav-item">
							<NavLink className="nav-link" to="/products">Products</NavLink>
						</li>
						<li className="nav-item">
							<NavLink className="nav-link" to="/contactUs">Contact Us</NavLink>
						</li>
					</ul>
	
					<ul className="navbar-nav ml-auto">
						<li className="nav-item">
							<NavLink className="nav-link" to="/carts">My Cart</NavLink>
						</li>
						<li className="nav-item">
							<NavLink className="nav-link" to="/">Welcome</NavLink>		
						</li>				
						<li className="nav-item">
							<NavLink className="nav-link" to="/">Logout</NavLink>				
						</li>
					</ul>
				</div>
			</div>
		</nav>
	)
	} else {
	return(
		<nav className="navbar navbar-expand-lg navbar-light bg-light">
			<div className="container">
				<a href="#" className="navbar-brand">Navbar</a>
				<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="content">
					<span className="navbar-toggler-icon"></span>
				</button>
				
				<div className="collapse navbar-collapse" id="content">
					<ul className="navbar-nav mr-auto">
						<li className="nav-item">
							<Link className="nav-link" to="/">Home</Link>
						</li>
						<li className="nav-item">
							<NavLink className="nav-link" to="/products">Products</NavLink>
						</li>
						<li className="nav-item">
							<NavLink className="nav-link" to="/contactUs">Contact Us</NavLink>
						</li>
					</ul>
	
					<ul className="navbar-nav ml-auto">
						<li className="nav-item">
							<NavLink className="nav-link" to="/carts">My Cart</NavLink>
						</li>					
						<li className="nav-item">
							<NavLink className="nav-link" to="/">LogIn</NavLink>		
						</li>
						<li className="nav-item">
							<NavLink className="nav-link" to="/">Register</NavLink>				
						</li>
					</ul>
				</div>
			</div>
		</nav>
	)
	}
}

export default Navbar