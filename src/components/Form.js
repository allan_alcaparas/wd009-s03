import React from "react"

const Form = () => {
	let x = 1;
	if(x>0){

		return(
			<form>
				<div className="form-group">
					<label htmlFor="email">Email address</label>
					<input type="email" className="form-control" id="email" aria-describedby="emailHelp"/>
				</div>
				<div className="form-group">
					<label htmlFor="password">Password</label>
					<input type="password" className="form-control" id="password"/>
				</div>
				<div className="form-group form-check">
					<label htmlFor="textarea">Example textarea</label>
			    	<textarea className="form-control" id="textarea" rows="3"></textarea>
				</div>
				<button type="submit" className="btn btn-primary">Submit</button>
			</form>
		)
	} else {
		return(
			<h1>no need to send feedback</h1>
		)
	}
}

export default Form
