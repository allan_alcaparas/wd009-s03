import React from "react"

const Product = () => {
	return(
		<div className="container">
			<div className="row">
				<div className="col-12">
					<h1 className="text-center">Product Page</h1>
					<hr/>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, velit voluptate amet sapiente quod magnam earum facere. Officia ipsam, mollitia fuga quaerat doloribus in iusto architecto nam expedita, at beatae!</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, velit voluptate amet sapiente quod magnam earum facere. Officia ipsam, mollitia fuga quaerat doloribus in iusto architecto nam expedita, at beatae!</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, velit voluptate amet sapiente quod magnam earum facere. Officia ipsam, mollitia fuga quaerat doloribus in iusto architecto nam expedita, at beatae!</p>
				</div>
			</div>
		</div>
	)
}

export default Product