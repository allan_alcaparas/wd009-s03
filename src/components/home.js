import React from "react"

// create the home section using divs for container, row and col-12

const Home = () => {
	return(
		<div className="container">
			<div className="row">
				<div className="col-12">
					<h1 className="text-center">Home</h1>
					<hr/>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, velit voluptate amet sapiente quod magnam earum facere. Officia ipsam, mollitia fuga quaerat doloribus in iusto architecto nam expedita, at beatae!</p>
					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Culpa, velit voluptate amet sapiente quod magnam earum facere. Officia ipsam, mollitia fuga quaerat doloribus in iusto architecto nam expedita, at beatae!</p>
				</div>
			</div>
		</div>
	)
}

export default Home